import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import './index.css';
import App from './App';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import 'typeface-roboto'

export const themeOptions = {
  palette: {
    type: 'light',
    primary: {
      main: '#387fc9',
    },
    secondary: {
      main: '#f50057',
    },
    tertiary: {
      main: '#123456',
    },
  },
  props: {
    MuiList: {
      dense: true,
    },
    MuiMenuItem: {
      dense: true,
    },
    MuiTable: {
      size: 'small',
    },
    MuiButtonBase: {
      disableRipple: true,
    },
  },
};

const theme = createTheme(themeOptions);

ReactDOM.render(
  <BrowserRouter>
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </React.StrictMode>
  </BrowserRouter>,
  document.getElementById('root')
);
