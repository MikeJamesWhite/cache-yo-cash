import React from "react";
import { Routes, Route } from "react-router-dom";

import Overview from './pages/Overview'
import Deductions from './pages/Deductions'
import Expenses from './pages/Expenses'
import Income from './pages/Income'
import Transactions from './pages/Transactions'
import './App.css';
import TopBar from "./components/TopBar";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <TopBar />
        <Routes>
          <Route path="/" element={<Overview />} />
          <Route path="transactions" element={<Transactions />} />
          <Route path="income" element={<Income />} />
          <Route path="expenses" element={<Expenses />} />
          <Route path="deductions" element={<Deductions />} />
        </Routes>
      </div>
    );
  }
}

export default App;
