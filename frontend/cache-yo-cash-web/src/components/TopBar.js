import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { useNavigate } from 'react-router-dom';

const navLinks = [
  {
    label: "Overview",
    href: "/"
  },
  {
    label: "Transactions",
    href: "/transactions"
  },
  {
    label: "Income",
    href: "/income"
  },
  {
    label: "Expenses",
    href: "/expenses"
  },
  {
    label: "Deductions",
    href: "/deductions"
  }
];

function LinkButton(props) {
  const navigate = useNavigate()

  const routeChange = () => {
    navigate(props.href);
  }

  return (
    <Button
      component="a"
      variant="contained"
      color="primary"
      onClick={routeChange}
      {...props}
    >{props.label}</Button>
  );
}

export default function TopBar() {
  const buttons = [
    <LinkButton key={0} label={navLinks[0].label} href={navLinks[0].href} />,
    <LinkButton key={1} label={navLinks[1].label} href={navLinks[1].href} />,
    <LinkButton key={2} label={navLinks[2].label} href={navLinks[2].href} />,
    <LinkButton key={3} label={navLinks[3].label} href={navLinks[3].href} />,
    <LinkButton key={4} label={navLinks[4].label} href={navLinks[4].href} />
  ];

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar color='inherit' position="static">
        <Toolbar>
          <Box sx={{ width: '80px', height: '80px', mt: '10px', mb: '10px', mr: '10px' }}>
            <img src='./logo.png' alt="logo" height="80" width="80" />
          </Box>
          <Typography variant="h5" component="div" sx={{ alignContent: 'left' }}>
            <b>Cache Yo Cash!</b>
          </Typography>
          <ButtonGroup size="medium" aria-label="large button group" sx={{ flexGrow: 1, alignContent: 'right', justifyContent: 'right' }}>
            {buttons}
          </ButtonGroup>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
