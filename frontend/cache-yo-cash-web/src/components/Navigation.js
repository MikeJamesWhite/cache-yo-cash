import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useNavigate, useLocation } from 'react-router-dom';

const navLinks = [
    {
        label: "Home",
        href: "/"
    },
    {
        label: "Transactions",
        href: "/transactions"
    },
    {
        label: "Income",
        href: "/income"
    },
    {
        label: "Expenses",
        href: "/expenses"
    },
    {
        label: "Deductions",
        href: "/deductions"
    }
];

function LinkTab(props) {
    const routeChange = () => {
        props.navigate(props.href);
    }

    return (
        <Tab
            component="a"
            onClick={routeChange}
            {...props}
        />
    );
}

export default function Navigation() {
    const location = useLocation();

    const handleChange = (event, newTab) => {
        return;
    };

    const checkPath = (path) => {
        console.log(`location.pathname: ${location.pathname}`)
        console.log(`path: ${path.href}`)

        return location.pathname === path.href;
    }

    console.log(`index: ${navLinks.findIndex(checkPath)}`)

    return (
        <Box sx={{ width: '100%', alignContent: 'right' }}>
            <Tabs value={navLinks.findIndex(checkPath)} onChange={handleChange} aria-label="navigation-bar">
                <LinkTab label={navLinks[0].label} href={navLinks[0].href} />
                <LinkTab label={navLinks[1].label} href={navLinks[1].href} />
                <LinkTab label={navLinks[2].label} href={navLinks[2].href} />
                <LinkTab label={navLinks[3].label} href={navLinks[3].href} />
                <LinkTab label={navLinks[4].label} href={navLinks[4].href} />
            </Tabs>
        </Box>
    );
}
