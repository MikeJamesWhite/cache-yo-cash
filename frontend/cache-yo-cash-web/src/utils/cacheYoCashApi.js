import * as moment from 'moment';

const BASE_URL = 'http://localhost:5000';
const DEFAULT_START_DATE = moment().subtract(30, 'days');
const DEFAULT_END_DATE = moment();

export async function getIncome(component) {
    component.setState({
        income: {
            isLoaded: false,
            items: undefined
        }
    });

    const fromDate = moment(component.state.startDate ? component.state.startDate : DEFAULT_START_DATE).format('YYYY-MM-DD');
    const toDate = moment(component.state.endDate ? component.state.endDate : DEFAULT_END_DATE).format('YYYY-MM-DD');

    fetch(`${BASE_URL}/income?fromDate=${fromDate}&toDate=${toDate}`)
        .then(res => res.json())
        .then(result => {
            result.income.forEach((income) => {
                income.amount = parseFloat(income.amount);
                income.linkedTransactionPostedOrder = parseInt(income.linkedTransactionPostedOrder);
            });

            component.setState({
                income: {
                    isLoaded: true,
                    items: result.income
                }
            });
        });
}

export async function getExpenses(component) {
    component.setState({
        expenses: {
            isLoaded: false,
            items: undefined
        }
    });

    const fromDate = moment(component.state.startDate ? component.state.startDate : DEFAULT_START_DATE).format('YYYY-MM-DD');
    const toDate = moment(component.state.endDate ? component.state.endDate : DEFAULT_END_DATE).format('YYYY-MM-DD');

    fetch(`${BASE_URL}/expenses?fromDate=${fromDate}&toDate=${toDate}`)
        .then(res => res.json())
        .then(result => {
            result.expenses.forEach((expense) => {
                expense.amount = parseFloat(expense.amount);
                expense.linkedTransactionPostedOrder = parseInt(expense.linkedTransactionPostedOrder);
            });

            component.setState({
                expenses: {
                    isLoaded: true,
                    items: result.expenses
                }
            });
        });
}


export async function getDeductions(component) {
    component.setState({
        deductions: {
            isLoaded: false,
            items: undefined
        }
    });

    const fromDate = moment(component.state.startDate ? component.state.startDate : DEFAULT_START_DATE).format('YYYY-MM-DD');
    const toDate = moment(component.state.endDate ? component.state.endDate : DEFAULT_END_DATE).format('YYYY-MM-DD');

    fetch(`${BASE_URL}/deductions?fromDate=${fromDate}&toDate=${toDate}`)
        .then(res => res.json())
        .then(result => {
            result.deductions.forEach((deduction) => {
                deduction.amount = parseFloat(deduction.amount);
            });

            component.setState({
                deductions: {
                    isLoaded: true,
                    items: result.deductions
                }
            });
        });
}

export async function getTransactions(component) {
    component.setState({
        transactions: {
            isLoaded: false,
            items: undefined
        }
    });

    const fromDate = moment(component.state.startDate ? component.state.startDate : DEFAULT_START_DATE).format('YYYY-MM-DD');
    const toDate = moment(component.state.endDate ? component.state.endDate : DEFAULT_END_DATE).format('YYYY-MM-DD');

    fetch(`${BASE_URL}/transactions?fromDate=${fromDate}&toDate=${toDate}`)
        .then(res => res.json())
        .then(result => {
            result.transactions.forEach((transaction) => {
                transaction.amount = parseFloat(transaction.amount);
                transaction.postedOrder = parseInt(transaction.postedOrder);
            });

            component.setState({
                transactions: {
                    isLoaded: true,
                    items: result.transactions
                }
            });
        });
}

export async function getHoldingAccountBalance(component) {
    component.setState({
        holdingAccount: {
            isLoaded: false,
            balance: undefined
        }
    })

    fetch(`${BASE_URL}/holdingAccount`)
        .then(res => res.json())
        .then(result => {
            component.setState({
                holdingAccount: {
                    isLoaded: true,
                    balance: result.availableBalance
                }
            })
        });
}

export async function importTransactions(data) {
    if (data.startDate !== undefined && data.endDate !== undefined) {
        fetch(`${BASE_URL}/transactions`, {
            method: 'POST',
            body: JSON.stringify({
                fromDate: moment(data.startDate).format('YYYY-MM-DD'),
                toDate: moment(data.endDate).format('YYYY-MM-DD')
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                console.log(result)
            });

        return 'Success'
    }

    return 'Invalid request'
}


export async function addDeduction(data) {
    if (data.date !== undefined && data.description !== undefined && data.amount !== undefined) {
        var toReturn;
        fetch(`${BASE_URL}/deductions`, {
            method: 'POST',
            body: JSON.stringify({
                items: [{
                    date: moment(data.date).format('YYYY-MM-DD'),
                    amount: parseFloat(data.amount),
                    description: data.description
                }]
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.failedItems.length > 0) {
                    toReturn = "Failed to process item"
                } else if (result.invalidItems.length > 0) {
                    toReturn = "Invalid item"
                } else {
                    toReturn = "Success"
                }
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function addIncome(data) {
    if (data.date !== undefined && data.description !== undefined && data.amount !== undefined && data.isTaxable !== undefined) {
        var toReturn;
        var body;

        if (data.linkedTransactionPostedOrder !== undefined) {
            body = {
                items: [
                    {
                        date: moment(data.date).format('YYYY-MM-DD'),
                        amount: parseFloat(data.amount),
                        description: data.description,
                        isTaxable: data.isTaxable,
                        linkedTransactionPostedOrder: data.linkedTransactionPostedOrder
                    }
                ]
            }
        } else {
            body = {
                items: [
                    {
                        date: moment(data.date).format('YYYY-MM-DD'),
                        amount: parseFloat(data.amount),
                        description: data.description,
                        isTaxable: data.isTaxable,
                    }
                ]
            }
        }

        fetch(`${BASE_URL}/income`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.failedItems.length > 0) {
                    toReturn = "Failed to process item"
                } else if (result.invalidItems.length > 0) {
                    toReturn = "Invalid item"
                } else {
                    toReturn = "Success"
                }
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function addExpense(data) {
    if (data.date !== undefined && data.description !== undefined && data.amount !== undefined && data.isDeductible !== undefined) {
        var toReturn;
        var body;

        if (data.linkedTransactionPostedOrder !== undefined) {
            body = {
                items: [
                    {
                        date: moment(data.date).format('YYYY-MM-DD'),
                        amount: parseFloat(data.amount),
                        description: data.description,
                        isDeductible: data.isDeductible,
                        linkedTransactionPostedOrder: data.linkedTransactionPostedOrder
                    }
                ]
            }
        } else {
            body = {
                items: [
                    {
                        date: moment(data.date).format('YYYY-MM-DD'),
                        amount: parseFloat(data.amount),
                        description: data.description,
                        isDeductible: data.isDeductible
                    }
                ]
            }
        }

        fetch(`${BASE_URL}/expenses`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.failedItems.length > 0) {
                    toReturn = "Failed to process item"
                } else if (result.invalidItems.length > 0) {
                    toReturn = "Invalid item"
                } else {
                    toReturn = "Success"
                }
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function deleteIncome(incomeList) {
    if (incomeList !== undefined && incomeList.length > 0) {
        var toReturn;
        var body = {
            items: incomeList
        }

        fetch(`${BASE_URL}/income`, {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                toReturn = result;
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function deleteDeductions(deductionsList) {
    if (deductionsList !== undefined && deductionsList.length > 0) {
        var toReturn;
        var body = {
            items: deductionsList
        }

        fetch(`${BASE_URL}/deductions`, {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                toReturn = result;
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function deleteExpenses(expensesList) {
    if (expensesList !== undefined && expensesList.length > 0) {
        var toReturn;
        var body = {
            items: expensesList
        }

        fetch(`${BASE_URL}/expenses`, {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                toReturn = result;
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function deleteTransactions(transactionsList) {
    if (transactionsList !== undefined && transactionsList.length > 0) {
        var toReturn;
        var body = {
            items: transactionsList
        }

        fetch(`${BASE_URL}/transactions`, {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                toReturn = result;
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function updateTransactions(transactionsList) {
    if (transactionsList !== undefined && transactionsList.length > 0) {
        var toReturn;
        var body = {
            items: transactionsList
        }

        fetch(`${BASE_URL}/transactions`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                toReturn = result;
            });

        return toReturn;
    }

    return 'Invalid request'
}

export async function transferToHoldingAccount(amount) {
    if (amount) {
        var body = {
            amount
        }

        fetch(`${BASE_URL}/holdingAccount`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(result => {
                console.log(result);
            });

        return 'Success'
    }

    return 'Invalid request'
}
