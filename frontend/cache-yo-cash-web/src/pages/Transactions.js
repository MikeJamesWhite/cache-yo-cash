import * as React from 'react';
import TransactionsTable from '../components/TransactionsTable'
import '../App.css';
import { Box } from '@mui/system';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { CircularProgress, Container } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import RefreshIcon from '@mui/icons-material/Refresh';
import * as moment from 'moment';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Slider from '@mui/material/Slider';
import { getTransactions, importTransactions, addExpense, addIncome, transferToHoldingAccount, updateTransactions } from '../utils/cacheYoCashApi'

export default class Transactions extends React.Component {

  constructor(props) {
    super(props)
    this.defaultEndDate = moment();
    this.defaultStartDate = moment().subtract(30, 'days');

    this.state = {
      startDate: this.defaultStartDate,
      endDate: this.defaultEndDate,
      transactions: {
        isLoaded: false,
        items: undefined,
        isImporting: false
      },
      form: {
        startDate: moment().subtract(30, 'days'),
        endDate: moment()
      },
      processTransaction: {
        amount: 100.00,
        type: undefined,
        date: undefined,
        transferToHoldingAmount: 0.0,
        isTaxable: true,
        isDeductible: true,
        description: '',
        linkedTransactionPostedOrder: undefined
      },
      selectedTransaction: undefined,
      importTransactionsFormOpen: false,
      importTransactionsFormEditable: true,
      processTransactionFormOpen: false,
      processTransactionFormEditable: true
    }

    this.getTransactions = getTransactions.bind(this, this);
    this.importTransactions = importTransactions.bind(this, this)
    this.openImportTransactionsForm = this.openImportTransactionsForm.bind(this);
    this.closeImportTransactionsForm = this.closeImportTransactionsForm.bind(this);
    this.submitImportTransactionsForm = this.submitImportTransactionsForm.bind(this);

    this.setSelectedTransaction = this.setSelectedTransaction.bind(this);
    this.openProcessTransactionsForm = this.openProcessTransactionsForm.bind(this);
    this.closeProcessTransactionsForm = this.closeProcessTransactionsForm.bind(this);
    this.submitProcessTransactionsForm = this.submitProcessTransactionsForm.bind(this);
  }

  setSelectedTransaction(transaction) {
    if (transaction !== undefined) {
      this.setState({
        processTransaction: {
          amount: transaction.amount,
          type: transaction.type,
          date: moment(transaction.date),
          isTaxable: true,
          isDeductible: true,
          description: transaction.description,
          transferToHoldingAmount: 0.0,
          linkedTransactionPostedOrder: transaction.postedOrder
        },
        selectedTransaction: transaction
      })
    }
  }

  openProcessTransactionsForm() {
    this.setState({
      processTransactionFormOpen: true
    });
  }

  closeProcessTransactionsForm() {
    this.setState({
      processTransactionFormOpen: false
    });
  }

  async submitProcessTransactionsForm() {
    this.setState({
      processTransactionFormEditable: false
    });

    var res;
    if (this.state.processTransaction.type === 'DEBIT') {
      res = await addExpense(this.state.processTransaction)
    } else if (this.state.processTransaction.type === 'CREDIT') {
      res = await addIncome(this.state.processTransaction)

      if (this.state.processTransaction.transferToHoldingAmount > 0) {
        console.log(res);
        res = await transferToHoldingAccount(this.state.processTransaction.transferToHoldingAmount)
      }

    } else {
      console.log('Unrecognised transaction type');
    }
    console.log(res);

    this.state.selectedTransaction.hasBeenProcessed = true;
    res = await updateTransactions([this.state.selectedTransaction]);
    console.log(res);

    this.setState({
      selectedTransaction: undefined,
      processTransactionFormEditable: true,
      processTransactionFormOpen: false
    });

    getTransactions(this);
  }

  openImportTransactionsForm() {
    this.setState({
      importTransactionsFormOpen: true
    });
  }

  closeImportTransactionsForm() {
    this.setState({
      importTransactionsFormOpen: false
    });
  }

  async submitImportTransactionsForm() {
    this.setState({
      importTransactionsFormEditable: false
    });

    const res = await importTransactions(this.state.form)
    console.log(res);

    this.setState({
      form: {
        startDate: moment().subtract(30, 'days'),
        endDate: moment()
      },
      importTransactionsFormEditable: true,
      importTransactionsFormOpen: false
    });

    getTransactions(this);
  }

  getImportTransactionsFormModal() {
    return (
      <div>
        <Dialog open={this.state.importTransactionsFormOpen} onClose={this.closeImportTransactionsForm}>
          <DialogTitle>Import transactions with the Investec API</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To import transactions, please select the date range below and then click submit.
            </DialogContentText>
            <Stack direction="row" spacing={3} sx={{ marginTop: '32px' }}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label='Select start date'
                  value={this.state.form.startDate}
                  onChange={(newValue) => {
                    var form = {
                      ...this.state.form,
                      startDate: newValue
                    }
                    this.setState({
                      form
                    });
                  }}
                  renderInput={(params) => (
                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                  )}
                  disabled={!this.state.importTransactionsFormEditable}
                />
                <DatePicker
                  label='Select end date'
                  value={this.state.form.endDate}
                  onChange={(newValue) => {
                    var form = {
                      ...this.state.form,
                      endDate: newValue
                    }
                    this.setState({
                      form
                    });
                  }}
                  renderInput={(params) => (
                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                  )}
                  disabled={!this.state.importTransactionsFormEditable}
                />
              </LocalizationProvider>
            </Stack>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeImportTransactionsForm} disabled={!this.state.importTransactionsFormEditable}>Cancel</Button>
            <Button onClick={this.submitImportTransactionsForm} disabled={!this.state.importTransactionsFormEditable}>Submit</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

  getProcessTransactionFormModal() {
    const valuetext = (value) => {
      return `R${value.toFixed(2)}`;
    }

    const updateTransferAmount = (event, newValue) => {
      var processTransaction = {
        ...this.state.processTransaction,
        transferToHoldingAmount: newValue
      }
      this.setState({
        processTransaction
      });
    }

    return (
      <div>
        <Dialog open={this.state.processTransactionFormOpen} onClose={this.closeProcessTransactionForm}>
          <DialogTitle>Process a transaction</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To process the transaction into a {this.state.selectedTransaction && this.state.selectedTransaction.type === 'DEBIT' ? 'expense' : 'income'}, please fill in the details below and then click submit.
            </DialogContentText>
            <Box sx={{ mt: '32px' }}>
            </Box>
            <Stack direction="row" spacing={3} sx={{ marginTop: '16px' }}>
              <FormControl>
                <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-amount"
                  value={this.state.processTransaction.amount}
                  onChange={this.onChangeAmount}
                  fullWidth
                  startAdornment={<InputAdornment position="start">ZAR</InputAdornment>}
                  label="Amount"
                  disabled
                />
              </FormControl>
              <FormGroup>
                {this.state.processTransaction.type === 'CREDIT' && <FormControlLabel
                  control={
                    <Checkbox checked={this.state.processTransaction.isTaxable} onChange={(event) => {
                      var processTransaction = {
                        ...this.state.processTransaction,
                        isTaxable: event.target.checked
                      }
                      this.setState({
                        processTransaction
                      });
                    }} />
                  }
                  label="Taxable"
                />}
                {this.state.processTransaction.type === 'DEBIT' && <FormControlLabel
                  control={
                    <Checkbox checked={this.state.processTransaction.isDeductible} onChange={(event) => {
                      var processTransaction = {
                        ...this.state.processTransaction,
                        isDeductible: event.target.checked
                      }
                      this.setState({
                        processTransaction
                      });
                    }} />
                  }
                  label="Deductible"
                />}
              </FormGroup>
            </Stack>
            {this.state.processTransaction.type === 'CREDIT' && <Box sx={{ mt: '32px', mr: '32px' }}>
              <DialogContentText>
                If you'd like to automatically transfer some portion of the income to the tax holding account, drag the slider across.
              </DialogContentText>
              <Slider
                aria-label="Always visible"
                min={0.0}
                value={this.state.processTransaction.transferToHoldingAmount || 0.0}
                onChange={updateTransferAmount}
                max={this.state.processTransaction.amount * 0.28}
                valueLabelFormat={valuetext}
                valueLabelDisplay="on"
              />
            </Box>}
            <Box sx={{ mt: '32px' }}>
              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-amount">Description</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-amount"
                  value={this.state.processTransaction.description}
                  onChange={(event) => {
                    var form = {
                      ...this.state.form,
                      description: event.target.value
                    }
                    this.setState({
                      form
                    });
                  }}
                  fullWidth
                  label="Description"
                  margin="dense"
                  disabled={!this.state.processTransactionFormEditable}
                />
              </FormControl>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeProcessTransactionsForm} disabled={!this.state.processTransactionFormEditable}>Cancel</Button>
            <Button onClick={this.submitProcessTransactionsForm} disabled={!this.state.processTransactionFormEditable}>Submit</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

  componentDidMount() {
    getTransactions(this)
  }

  render() {
    return (
      <Container>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Stack direction="row" spacing={2} sx={{ marginTop: '36px' }}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label='Select start date'
                value={this.state.startDate}
                onChange={(newValue) => {
                  this.setState({
                    startDate: newValue
                  });
                }}
                renderInput={(params) => (
                  <TextField {...params} helperText={params?.inputProps?.placeholder} />
                )}
              />
              <DatePicker
                label='Select end date'
                value={this.state.endDate}
                onChange={(newValue) => {
                  this.setState({
                    endDate: newValue
                  });
                }}
                renderInput={(params) => (
                  <TextField {...params} helperText={params?.inputProps?.placeholder} />
                )}
                sx={{ marginRight: '24px' }}
              />
            </LocalizationProvider>
            <div>
              {this.state.transactions.isLoaded &&
                <IconButton
                  size="large"
                  color="primary"
                  onClick={this.getTransactions}>
                  <RefreshIcon />
                </IconButton>}
              {!this.state.transactions.isLoaded && <CircularProgress />}
            </div>
          </Stack>
        </Box>

        <Box display='flex' sx={{ marginTop: '12px' }}>
          {this.state.transactions.isLoaded && this.state.transactions.items.length > 0 && <TransactionsTable data={this.state.transactions.items} openImportTransactionsForm={this.openImportTransactionsForm} openProcessTransactionsForm={this.openProcessTransactionsForm} setSelectedTransaction={this.setSelectedTransaction} />}
          {this.state.transactions.isLoaded && this.state.transactions.items.length <= 0 && <p>No data to render</p>}
          {!this.state.transactions.isLoaded && <Container><CircularProgress /></Container>}
        </Box>

        {this.getImportTransactionsFormModal()}
        {this.getProcessTransactionFormModal()}
      </Container>
    );
  }
}
