import * as React from 'react';
import IncomeTable from '../components/IncomeTable'
import '../App.css';
import { Box } from '@mui/system';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { CircularProgress, Container } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import RefreshIcon from '@mui/icons-material/Refresh';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import * as moment from 'moment';
import { getIncome, addIncome } from '../utils/cacheYoCashApi'


export default class Income extends React.Component {
  constructor(props) {
    super(props)
    this.defaultEndDate = moment();
    this.defaultStartDate = moment().subtract(30, 'days');

    this.state = {
      startDate: this.defaultStartDate,
      endDate: this.defaultEndDate,
      income: {
        isLoaded: false,
        items: undefined
      },
      form: {
        amount: 0.0,
        description: '',
        date: moment(),
        isTaxable: true
      },
      incomeFormOpen: false,
      incomeFormEditable: true
    }

    this.getIncome = getIncome.bind(this, this);
    this.openIncomeForm = this.openIncomeForm.bind(this);
    this.closeIncomeForm = this.closeIncomeForm.bind(this);
    this.submitIncomeForm = this.submitIncomeForm.bind(this);
    this.onChangeAmount = this.onChangeAmount.bind(this);
  }

  componentDidMount() {
    getIncome(this)
  }

  openIncomeForm() {
    this.setState({
      incomeFormOpen: true
    })
  }

  closeIncomeForm() {
    this.setState({
      incomeFormOpen: false
    })
  }

  async submitIncomeForm() {
    this.setState({
      incomeFormEditable: false
    });

    const res = await addIncome(this.state.form);
    console.log(res);

    this.setState({
      form: {
        amount: 0.0,
        description: '',
        date: moment(),
        isTaxable: true
      },
      incomeFormEditable: true,
      incomeFormOpen: false
    });

    getIncome(this);
  }

  onChangeAmount(event) {
    const re = /^\d{0,10}(\.\d{0,2})?$/;

    // if value is not blank, then test the regex
    if (event.target.value === '' || re.test(event.target.value)) {
      if (event.target.value.charAt(0) === '0' && !(event.target.value.length === 1 || event.target.value.charAt(1) === '.')) {
        return;
      }

      var form = {
        ...this.state.form,
        amount: event.target.value
      };

      this.setState({
        form
      });
    }
  }

  getIncomeFormModal() {
    return (
      <div>
        <Dialog open={this.state.incomeFormOpen} onClose={this.closeIncomeForm}>
          <DialogTitle>Add an income</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To add an income, please fill in the details below and then click submit.
            </DialogContentText>
            <Box sx={{ mt: '32px' }}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label='Select income date'
                  value={this.state.form.date}
                  onChange={(newValue) => {
                    var form = {
                      ...this.state.form,
                      date: newValue
                    }
                    this.setState({
                      form
                    });
                  }}
                  renderInput={(params) => (
                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                  )}
                  disabled={!this.state.incomeFormEditable}
                />
              </LocalizationProvider>
            </Box>
            <Stack direction="row" spacing={3} sx={{ marginTop: '16px' }}>
              <FormControl>
                <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-amount"
                  value={this.state.form.amount}
                  onChange={this.onChangeAmount}
                  fullWidth
                  startAdornment={<InputAdornment position="start">ZAR</InputAdornment>}
                  label="Amount"
                  disabled={!this.state.incomeFormEditable}
                />
              </FormControl>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox checked={this.state.form.isTaxable} onChange={(event) => {
                      var form = {
                        ...this.state.form,
                        isTaxable: event.target.checked
                      }
                      this.setState({
                        form
                      });
                    }} />
                  }
                  label="Taxable"
                />
              </FormGroup>
            </Stack>
            <Box sx={{ mt: '32px' }}>
              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-amount">Description</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-amount"
                  value={this.state.form.description}
                  onChange={(event) => {
                    var form = {
                      ...this.state.form,
                      description: event.target.value
                    }
                    this.setState({
                      form
                    });
                  }}
                  fullWidth
                  label="Description"
                  margin="dense"
                  disabled={!this.state.incomeFormEditable}
                />
              </FormControl>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeIncomeForm} disabled={!this.state.incomeFormEditable}>Cancel</Button>
            <Button onClick={this.submitIncomeForm} disabled={!this.state.incomeFormEditable}>Submit</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

  render() {
    return (
      <Container>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Stack direction="row" spacing={2} sx={{ marginTop: '36px' }}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label='Select start date'
                value={this.state.startDate}
                onChange={(newValue) => {
                  this.setState({
                    startDate: newValue
                  });
                }}
                renderInput={(params) => (
                  <TextField {...params} helperText={params?.inputProps?.placeholder} />
                )}
              />
              <DatePicker
                label='Select end date'
                value={this.state.endDate}
                onChange={(newValue) => {
                  this.setState({
                    endDate: newValue
                  });
                }}
                renderInput={(params) => (
                  <TextField {...params} helperText={params?.inputProps?.placeholder} />
                )}
                sx={{ marginRight: '24px' }}
              />
            </LocalizationProvider>
            <div>
              {this.state.income.isLoaded &&
                <IconButton
                  size="large"
                  color="primary"
                  onClick={this.getIncome}>
                  <RefreshIcon />
                </IconButton>}
              {!this.state.income.isLoaded && <CircularProgress />}
            </div>
          </Stack>
        </Box>

        <Box display='flex' sx={{ marginTop: '12px' }}>
          {this.state.income.isLoaded && this.state.income.items.length > 0 && <IncomeTable data={this.state.income.items} openIncomeForm={this.openIncomeForm} />}
          {this.state.income.isLoaded && this.state.income.items.length <= 0 && <p>No data to render</p>}
          {!this.state.income.isLoaded && <Container><CircularProgress /></Container>}
        </Box>

        {this.getIncomeFormModal()}
      </Container>
    );
  }
}
