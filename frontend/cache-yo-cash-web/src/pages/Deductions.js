import DeductionsTable from '../components/DeductionsTable'
import * as React from 'react';
import '../App.css';
import { Box } from '@mui/system';
import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { CircularProgress, Container } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import RefreshIcon from '@mui/icons-material/Refresh';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import * as moment from 'moment';
import { getDeductions, addDeduction } from '../utils/cacheYoCashApi'

export default class Deductions extends React.Component {
    constructor(props) {
        super(props)
        this.defaultEndDate = moment();
        this.defaultStartDate = moment().subtract(30, 'days');

        this.state = {
            startDate: this.defaultStartDate,
            endDate: this.defaultEndDate,
            deductions: {
                isLoaded: false,
                items: undefined
            },
            form: {
                amount: 0.0,
                description: '',
                date: moment()
            },
            deductionsFormOpen: false,
            deductionsFormEditable: true,
        }

        this.addDeduction = addDeduction.bind(this)
        this.getDeductions = getDeductions.bind(this, this);
        this.openDeductionsForm = this.openDeductionsForm.bind(this);
        this.closeDeductionsForm = this.closeDeductionsForm.bind(this);
        this.submitDeductionsForm = this.submitDeductionsForm.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this)
    }

    componentDidMount() {
        getDeductions(this)
    }

    openDeductionsForm() {
        this.setState({
            deductionsFormOpen: true
        })
    }

    closeDeductionsForm() {
        this.setState({
            deductionsFormOpen: false
        })
    }

    async submitDeductionsForm() {
        this.setState({
            deductionsFormEditable: false
        });

        const res = await addDeduction(this.state.form);
        console.log(res);

        this.setState({
            form: {
                amount: 0.0,
                description: '',
                date: moment()
            },
            deductionsFormEditable: true,
            deductionsFormOpen: false
        });

        getDeductions(this);
    }

    onChangeAmount(event) {
        const re = /^\d{0,10}(\.\d{0,2})?$/;

        // if value is not blank, then test the regex
        if (event.target.value === '' || re.test(event.target.value)) {
            if (event.target.value.charAt(0) === '0' && !(event.target.value.length === 1 || event.target.value.charAt(1) === '.')) {
                return;
            }

            var form = {
                ...this.state.form,
                amount: event.target.value
            };

            this.setState({
                form
            });
        }
    }

    getDeductionFormModal() {
        return (
            <div>
                <Dialog open={this.state.deductionsFormOpen} onClose={this.closeDeductionsForm}>
                    <DialogTitle>Add a deduction</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To add a deduction, please fill in the details below and then click submit.
                        </DialogContentText>
                        <Stack direction="row" spacing={3} sx={{ marginTop: '32px' }}>
                            <FormControl>
                                <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-amount"
                                    value={this.state.form.amount}
                                    onChange={this.onChangeAmount}
                                    fullWidth
                                    startAdornment={<InputAdornment position="start">ZAR</InputAdornment>}
                                    label="Amount"
                                    disabled={!this.state.deductionsFormEditable}
                                />
                            </FormControl>
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <DatePicker
                                    label='Select deduction date'
                                    value={this.state.form.date}
                                    onChange={(newValue) => {
                                        var form = {
                                            ...this.state.form,
                                            date: newValue
                                        }
                                        this.setState({
                                            form
                                        });
                                    }}
                                    renderInput={(params) => (
                                        <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                    )}
                                    disabled={!this.state.deductionsFormEditable}
                                />
                            </LocalizationProvider>
                        </Stack>
                        <Box sx={{ mt: '16px' }}>
                            <FormControl fullWidth>
                                <InputLabel htmlFor="outlined-adornment-amount">Description</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-amount"
                                    value={this.state.form.description}
                                    onChange={(event) => {
                                        var form = {
                                            ...this.state.form,
                                            description: event.target.value
                                        }
                                        this.setState({
                                            form
                                        });
                                    }}
                                    fullWidth
                                    label="Description"
                                    margin="dense"
                                    disabled={!this.state.deductionsFormEditable}
                                />
                            </FormControl>
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.closeDeductionsForm} disabled={!this.state.deductionsFormEditable}>Cancel</Button>
                        <Button onClick={this.submitDeductionsForm} disabled={!this.state.deductionsFormEditable}>Submit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }

    render() {
        return (
            <Container>
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Stack direction="row" spacing={2} sx={{ marginTop: '36px' }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                label='Select start date'
                                value={this.state.startDate}
                                onChange={(newValue) => {
                                    this.setState({
                                        startDate: newValue
                                    });
                                }}
                                renderInput={(params) => (
                                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                )}
                            />
                            <DatePicker
                                label='Select end date'
                                value={this.state.endDate}
                                onChange={(newValue) => {
                                    this.setState({
                                        endDate: newValue
                                    });
                                }}
                                renderInput={(params) => (
                                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                )}
                                sx={{ marginRight: '24px' }}
                            />
                        </LocalizationProvider>
                        <div>
                            {this.state.deductions.isLoaded &&
                                <IconButton
                                    size="large"
                                    color="primary"
                                    onClick={this.getDeductions}>
                                    <RefreshIcon />
                                </IconButton>}
                            {!this.state.deductions.isLoaded && <CircularProgress />}
                        </div>
                    </Stack>
                </Box>

                <Box display='flex' sx={{ marginTop: '12px' }}>
                    {this.state.deductions.isLoaded && this.state.deductions.items.length > 0 && <DeductionsTable data={this.state.deductions.items} openDeductionsForm={this.openDeductionsForm} />}
                    {this.state.deductions.isLoaded && this.state.deductions.items.length <= 0 && <p>No data to render</p>}
                    {!this.state.deductions.isLoaded && <Container><CircularProgress /></Container>}
                </Box>

                {this.getDeductionFormModal()}
            </Container>
        );
    }
}
