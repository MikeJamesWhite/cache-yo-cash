import { CircularProgress, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import '../App.css';
import * as moment from 'moment';
import { getIncome, getExpenses, getDeductions, getHoldingAccountBalance, transferToHoldingAccount } from '../utils/cacheYoCashApi'
import { Bar } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import IconButton from '@mui/material/IconButton';
import SavingsIcon from '@mui/icons-material/Savings';
import RefreshIcon from '@mui/icons-material/Refresh';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Slider from '@mui/material/Slider';
import Tooltip from '@mui/material/Tooltip';



Chart.register(...registerables, ChartDataLabels);

export default class Overview extends React.Component {
    constructor(props) {
        super(props)
        this.defaultEndDate = moment("2022-02-28");
        this.defaultStartDate = moment("2022-02-28").subtract(1, 'years').add(1, 'days');

        this.state = {
            startDate: this.defaultStartDate,
            endDate: this.defaultEndDate,
            income: {
                isLoaded: false,
                items: undefined
            },
            expenses: {
                isLoaded: false,
                items: undefined
            },
            deductions: {
                isLoaded: false,
                items: undefined
            },
            holdingAccount: {
                isLoaded: false,
                balance: undefined
            },
            transferToHoldingAccount: {
                amount: 0.0
            },
            transferFormEditable: true,
            transferFormOpen: false
        }
        this.getHoldingAccountBalance = getHoldingAccountBalance.bind(this, this);
        this.getIncome = getIncome.bind(this, this);
        this.getExpenses = getExpenses.bind(this, this);
        this.getDeductions = getDeductions.bind(this, this);
        this.refreshData = this.refreshData.bind(this);
        this.closeTransferForm = this.closeTransferForm.bind(this);
        this.openTransferForm = this.openTransferForm.bind(this);
        this.submitTransferForm = this.submitTransferForm.bind(this);
        this.transferToHoldingAccount = transferToHoldingAccount.bind(this)
    }

    componentDidMount() {
        this.refreshData()
    }

    async refreshData() {
        getHoldingAccountBalance(this)
        getIncome(this)
        getExpenses(this)
        getDeductions(this)
    }

    closeTransferForm() {
        this.setState({
            transferFormOpen: false
        });
    }

    openTransferForm() {
        this.setState({
            transferFormOpen: true
        });
    }

    async submitTransferForm() {
        this.setState({
            transferFormEditable: false
        });

        await this.transferToHoldingAccount(this.state.transferToHoldingAccount.amount);
        this.refreshData()

        this.setState({
            transferFormEditable: true,
            transferFormOpen: false
        });
    }


    isCurrentlyRefreshing() {
        return !(this.state.deductions.items && this.state.expenses.items && this.state.income.items && this.state.holdingAccount.balance !== undefined);
    }

    getTotalIncomeAmount() {
        if (this.state.income.items) {
            var total = 0.0;
            this.state.income.items.forEach((income) => {
                total += income.amount;
            });

            return total;
        } else {
            return undefined;
        }
    }

    getTaxableIncomeAmount() {
        if (this.state.income.items) {
            var total = 0.0;
            this.state.income.items.forEach((income) => {
                if (income.isTaxable) {
                    total += income.amount;
                }
            });

            return total;
        } else {
            return undefined;
        }
    }

    getTotalExpensesAmount() {
        if (this.state.expenses.items) {
            var total = 0.0;
            this.state.expenses.items.forEach((expense) => {
                total += expense.amount;
            });

            return total;
        } else {
            return undefined;
        }
    }

    getDeductibleExpensesAmount() {
        if (this.state.expenses.items) {
            var total = 0.0;
            this.state.expenses.items.forEach((expense) => {
                if (expense.isDeductible) {
                    total += expense.amount;
                }
            });

            return total;
        } else {
            return undefined;
        }
    }

    getTotalDeductionsAmount() {
        if (this.state.deductions.items) {
            var total = 0.0;
            this.state.deductions.items.forEach((deduction) => {
                total += deduction.amount;
            });

            return total;
        } else {
            return undefined;
        }
    }

    getTotalProfit() {
        if (this.state.deductions.items && this.state.expenses.items && this.state.income.items) {
            var profit = 0.0;
            this.state.income.items.forEach((income) => {
                if (income.isTaxable) {
                    profit += income.amount;
                }
            });

            this.state.expenses.items.forEach((expense) => {
                if (expense.isDeductible) {
                    profit -= expense.amount;
                }
            });

            this.state.deductions.items.forEach((deduction) => {
                profit -= deduction.amount;
            });

            return profit;
        } else {
            return undefined;
        }
    }

    getTaxOnProfit(profit) {
        return profit * 0.28;
    }

    getTotalTaxLiability() {
        if (this.state.deductions.items && this.state.expenses.items && this.state.income.items) {
            const profit = this.getTotalProfit();

            if (profit > 0) {
                return this.getTaxOnProfit(profit)
            } else {
                return 0.0;
            }
        } else {
            return undefined;
        }
    }

    getBreakdownBarChart() {
        if (this.state.deductions.items && this.state.expenses.items && this.state.income.items && this.state.holdingAccount.balance !== undefined) {
            const data = {
                labels: [''],
                datasets: [{
                    label: 'Taxable Income',
                    backgroundColor: '#B5EAD7',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.getTaxableIncomeAmount()],
                }, {
                    label: 'Deductible Expenses',
                    backgroundColor: '#FFDAC1',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.getDeductibleExpensesAmount()],
                }, {
                    label: 'Deductions',
                    backgroundColor: '#C7CEEA',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.getTotalDeductionsAmount()],
                }, {
                    label: this.getTotalProfit() < 0 ? "Loss" : "Profit",
                    backgroundColor: this.getTotalProfit() < 0 ? "#FFB7B2" : "#E2F0CB",
                    borderColor: 'rgb(0, 0, 0)',
                    data: [Math.abs(this.getTotalProfit())],
                }, {
                    label: "Tax liability",
                    backgroundColor: '#C8566B',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.getTotalTaxLiability()],
                }]
            };

            return (
                <Bar data={data} options={{
                    plugins: {
                        datalabels: {
                            display: true,
                            color: 'black',
                            formatter: function (value, context) {
                                return `R${value.toFixed(2)}`;
                            }
                        }
                    }
                }} />
            )
        } else {
            return (
                <CircularProgress />
            )
        }
    }

    getTaxBalanceBarChart() {
        if (this.getTotalTaxLiability() !== undefined && this.state.holdingAccount.balance !== undefined) {
            const data = {
                labels: [''],
                datasets: [{
                    label: "Tax liability",
                    backgroundColor: '#C8566B',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.getTotalTaxLiability()],
                }, {
                    label: "Holding account balance",
                    backgroundColor: '#B5EAD7',
                    borderColor: 'rgb(0, 0, 0)',
                    data: [this.state.holdingAccount.balance],
                }]
            };

            return (
                <Bar data={data} options={{
                    plugins: {
                        datalabels: {
                            display: true,
                            color: 'black',
                            formatter: function (value, context) {
                                return `R${value.toFixed(2)}`;
                            }
                        }
                    }
                }} />
            )
        } else {
            return (
                <CircularProgress />
            )
        }
    }

    getTransferFormModal() {
        const valuetext = (value) => {
            return `R${value.toFixed(2)}`;
        }

        const updateTransferAmount = (event, newValue) => {
            this.setState({
                transferToHoldingAccount: {
                    amount: newValue
                }
            })
        }

        return (
            <div>
                <Dialog open={this.state.transferFormOpen} onClose={this.closeTransferForm}>
                    <DialogTitle>Perform transfer to holding account</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To perform a transfer, please select the amount with the slider below and then click submit.
                        </DialogContentText>
                        <Box sx={{ mt: '32px', mr: '32px' }}>
                            <Slider
                                aria-label="Always visible"
                                min={0.01}
                                value={this.state.transferToHoldingAccount.amount}
                                onChange={updateTransferAmount}
                                max={this.getTotalTaxLiability() - this.state.holdingAccount.balance}
                                step={10}
                                valueLabelFormat={valuetext}
                                valueLabelDisplay="on"
                            />
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.closeTransferForm} disabled={!this.state.transferFormEditable}>Cancel</Button>
                        <Button onClick={this.submitTransferForm} disabled={!this.state.transferFormEditable}>Submit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }


    render() {
        return (
            <Box sx={{ padding: '12px' }}>
                {this.getTransferFormModal()}
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Stack direction="row" spacing={2} sx={{ marginTop: '36px' }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                label='Select end of the financial year'
                                value={this.state.endDate}
                                onChange={(newValue) => {
                                    this.setState({
                                        startDate: moment(newValue).subtract(1, 'years').add(1, 'days'),
                                        endDate: newValue
                                    });
                                }}
                                renderInput={(params) => (
                                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                )}
                                sx={{ marginRight: '24px' }}
                            />
                        </LocalizationProvider>
                        <div>
                            {!this.isCurrentlyRefreshing() &&
                                <IconButton
                                    size="large"
                                    color="primary"
                                    onClick={this.refreshData}>
                                    <RefreshIcon />
                                </IconButton>}
                            {this.isCurrentlyRefreshing() && <CircularProgress />}
                        </div>
                        <div>
                            {!this.isCurrentlyRefreshing() &&
                                <Tooltip title="Transfer to holding account">
                                    <IconButton
                                        size="large"
                                        color="primary"
                                        onClick={this.openTransferForm}>
                                        <SavingsIcon />
                                    </IconButton>
                                </Tooltip>}
                        </div>
                    </Stack>
                </Box>
                <Box sx={{ padding: '12px' }}>
                    <Typography
                        sx={{ flex: '1 1 100%' }}
                        variant="h6"
                        id="title"
                        component="div"
                    >
                        Tax liability vs. holding account balance
                    </Typography>
                    {this.getTaxBalanceBarChart()}
                </Box>
                <Box sx={{ padding: '12px' }}>
                    <Typography
                        sx={{ flex: '1 1 100%' }}
                        variant="h6"
                        id="title"
                        component="div"
                    >
                        Breakdown
                    </Typography>
                    {this.getBreakdownBarChart()}
                </Box>
            </Box>
        );
    }
}
