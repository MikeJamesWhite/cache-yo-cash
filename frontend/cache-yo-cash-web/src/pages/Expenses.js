import ExpensesTable from '../components/ExpensesTable'
import * as React from 'react';
import '../App.css';
import { Box } from '@mui/system';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { CircularProgress, Container } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import RefreshIcon from '@mui/icons-material/Refresh';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import * as moment from 'moment';
import { getExpenses, addExpense, deleteExpenses } from '../utils/cacheYoCashApi'

export default class Expenses extends React.Component {
    constructor(props) {
        super(props)
        this.defaultEndDate = moment();
        this.defaultStartDate = moment().subtract(30, 'days');

        this.state = {
            startDate: this.defaultStartDate,
            endDate: this.defaultEndDate,
            expenses: {
                isLoaded: false,
                items: undefined
            },
            form: {
                amount: 0.0,
                description: '',
                date: moment(),
                isDeductible: true
            },
            expensesFormOpen: false,
            expensesFormEditable: true,
            deleteExpensesAlertOpen: false,
            deleteExpensesAlertEditable: true
        }

        this.getExpenses = getExpenses.bind(this, this);
        this.openExpensesForm = this.openExpensesForm.bind(this);
        this.closeExpensesForm = this.closeExpensesForm.bind(this);
        this.submitExpensesForm = this.submitExpensesForm.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);

        this.submitDeleteExpenses = this.submitDeleteExpenses.bind(this);
        this.setDeleteExpensesAlertOpen = this.setDeleteExpensesAlertOpen.bind(this);
        this.setDeleteExpensesAlertEditable = this.setDeleteExpensesAlertEditable.bind(this);
    }

    componentDidMount() {
        getExpenses(this)
    }

    setDeleteExpensesAlertOpen(value) {
        this.setState({
            deleteExpensesAlertOpen: value
        })
    }

    setDeleteExpensesAlertEditable(value) {
        this.setState({
            deleteExpensesAlertEditable: value
        })
    }

    openExpensesForm() {
        this.setState({
            expensesFormOpen: true
        })
    }

    closeExpensesForm() {
        this.setState({
            expensesFormOpen: false
        })
    }

    async submitExpensesForm() {
        this.setState({
            expensesFormEditable: false
        });

        const res = await addExpense(this.state.form);
        console.log(res);

        this.setState({
            form: {
                amount: 0.0,
                description: '',
                date: moment(),
                isDeductible: true
            },
            expensesFormEditable: true,
            expensesFormOpen: false,
            deleteExpensesAlertOpen: false,
            deleteExpensesAlertEditable: true
        });

        getExpenses(this);
    }

    async submitDeleteExpenses(selectedExpenses) {
        const res = await deleteExpenses(selectedExpenses);
        console.log(res);

        getExpenses(this);
    }

    onChangeAmount(event) {
        const re = /^\d{0,10}(\.\d{0,2})?$/;

        // if value is not blank, then test the regex
        if (event.target.value === '' || re.test(event.target.value)) {
            if (event.target.value.charAt(0) === '0' && !(event.target.value.length === 1 || event.target.value.charAt(1) === '.')) {
                return;
            }

            var form = {
                ...this.state.form,
                amount: event.target.value
            };

            this.setState({
                form
            });
        }
    }

    getExpensesFormModal() {
        return (
            <div>
                <Dialog open={this.state.expensesFormOpen} onClose={this.closeExpensesForm}>
                    <DialogTitle>Add an expense</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To add an expense, please fill in the details below and then click submit.
                        </DialogContentText>
                        <Box sx={{ mt: '32px' }}>
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <DatePicker
                                    label='Select expense date'
                                    value={this.state.form.date}
                                    onChange={(newValue) => {
                                        var form = {
                                            ...this.state.form,
                                            date: newValue
                                        }
                                        this.setState({
                                            form
                                        });
                                    }}
                                    renderInput={(params) => (
                                        <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                    )}
                                    disabled={!this.state.expensesFormEditable}
                                />
                            </LocalizationProvider>
                        </Box>
                        <Stack direction="row" spacing={3} sx={{ marginTop: '16px' }}>
                            <FormControl>
                                <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-amount"
                                    value={this.state.form.amount}
                                    onChange={this.onChangeAmount}
                                    fullWidth
                                    startAdornment={<InputAdornment position="start">ZAR</InputAdornment>}
                                    label="Amount"
                                    disabled={!this.state.expensesFormEditable}
                                />
                            </FormControl>
                            <FormGroup>
                                <FormControlLabel
                                    control={
                                        <Checkbox checked={this.state.form.isDeductible} onChange={(event) => {
                                            var form = {
                                                ...this.state.form,
                                                isDeductible: event.target.checked
                                            }
                                            this.setState({
                                                form
                                            });
                                        }} />
                                    }
                                    label="Deductible"
                                />
                            </FormGroup>
                        </Stack>
                        <Box sx={{ mt: '32px' }}>
                            <FormControl fullWidth>
                                <InputLabel htmlFor="outlined-adornment-amount">Description</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-amount"
                                    value={this.state.form.description}
                                    onChange={(event) => {
                                        var form = {
                                            ...this.state.form,
                                            description: event.target.value
                                        }
                                        this.setState({
                                            form
                                        });
                                    }}
                                    fullWidth
                                    label="Description"
                                    margin="dense"
                                    disabled={!this.state.expensesFormEditable}
                                />
                            </FormControl>
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.closeExpensesForm} disabled={!this.state.expensesFormEditable}>Cancel</Button>
                        <Button onClick={this.submitExpensesForm} disabled={!this.state.expensesFormEditable}>Submit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }

    render() {
        return (
            <Container>
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Stack direction="row" spacing={2} sx={{ marginTop: '36px' }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                label='Select start date'
                                value={this.state.startDate}
                                onChange={(newValue) => {
                                    this.setState({
                                        startDate: newValue
                                    });
                                }}
                                renderInput={(params) => (
                                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                )}
                            />
                            <DatePicker
                                label='Select end date'
                                value={this.state.endDate}
                                onChange={(newValue) => {
                                    this.setState({
                                        endDate: newValue
                                    });
                                }}
                                renderInput={(params) => (
                                    <TextField {...params} helperText={params?.inputProps?.placeholder} />
                                )}
                                sx={{ marginRight: '24px' }}
                            />
                        </LocalizationProvider>
                        <div>
                            {this.state.expenses.isLoaded &&
                                <IconButton
                                    size="large"
                                    color="primary"
                                    onClick={this.getExpenses}>
                                    <RefreshIcon />
                                </IconButton>}
                            {!this.state.expenses.isLoaded && <CircularProgress />}
                        </div>
                    </Stack>
                </Box>

                <Box display='flex' sx={{ marginTop: '12px' }}>
                    {this.state.expenses.isLoaded && this.state.expenses.items.length > 0 && <ExpensesTable
                        data={this.state.expenses.items}
                        openExpensesForm={this.openExpensesForm}
                        setDeleteExpensesAlertEditable={this.setDeleteExpensesAlertEditable}
                        setDeleteExpensesAlertOpen={this.setDeleteExpensesAlertOpen}
                        submitDeleteExpenses={this.submitDeleteExpenses}
                        deleteExpensesAlertOpen={this.state.deleteExpensesAlertOpen}
                        deleteExpensesAlertEditable={this.state.deleteExpensesAlertEditable}
                    />}
                    {this.state.expenses.isLoaded && this.state.expenses.items.length <= 0 && <p>No data to render</p>}
                    {!this.state.expenses.isLoaded && <Container><CircularProgress /></Container>}
                </Box>

                {this.getExpensesFormModal()}
            </Container>
        );
    }
}