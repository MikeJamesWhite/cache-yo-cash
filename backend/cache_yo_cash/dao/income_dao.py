from decimal import Decimal
import json
import typing as tp
import os
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError
import logging

from cache_yo_cash.dao._dynamodb_dao import DynamoDBDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


class IncomeDao(DynamoDBDao):
    def __init__(self) -> None:
        super().__init__(os.getenv("INCOME_TABLE_NAME"))


    def put_items_if_not_exists(self, items: tp.List[tp.Dict]):
        return list(map(self.put_item_if_not_exists, items))


    def put_item_if_not_exists(self, item):
        item = json.loads(json.dumps(item), parse_float=Decimal)

        try:
            self.ddb_table.put_item(
                Item=item,
                ConditionExpression='attribute_not_exists(id)'
            )
            return {
                'item': item
            }
        except ClientError as e:
            logger.error(e)
            return {
                'item': item,
                'error': True
            }


    def delete_items(self, items: tp.List[tp.Dict]):
        return list(map(self.delete_item, items))


    def delete_item(self, item):
        try:
            self.ddb_table.delete_item(
                Key={
                    'id': item['id'],
                    'date': item['date']
                }
            )
            return {
                'item': item
            }
        except ClientError as e:
            logger.error(e)
            return {
                'item': item,
                'error': True
            }


    def scan_by_dates(self, from_date, to_date):
        scan_kwargs = {
            'FilterExpression': Attr('date').between(str(from_date), str(to_date)),
        }

        done = False
        start_key = None
        income = []

        while not done:
            if start_key:
                scan_kwargs['ExclusiveStartKey'] = start_key
            response = self.ddb_table.scan(**scan_kwargs)
            income += response.get('Items', [])
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None

        return income
 