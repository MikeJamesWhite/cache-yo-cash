from decimal import Decimal
import json
import typing as tp
import os
import logging
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError

from cache_yo_cash.dao._dynamodb_dao import DynamoDBDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


class TransactionsDao(DynamoDBDao):
    def __init__(self) -> None:
        super().__init__(os.getenv("TRANSACTIONS_TABLE_NAME"))


    def put_items_if_not_exists(self, items: tp.List[tp.Dict]):
        return list(map(self.put_item_if_not_exists, items))


    def put_item_if_not_exists(self, item):
        item = json.loads(json.dumps(item), parse_float=Decimal)

        try:
            self.ddb_table.put_item(
                Item=item,
                ConditionExpression='attribute_not_exists(postedOrder)'
            )
            return {
                'postedOrder': item['postedOrder'],
                'persisted': True
            }
        except ClientError as e:
            # Ignore the ConditionalCheckFailedException, bubble up other exceptions.
            if e.response['Error']['Code'] != 'ConditionalCheckFailedException':
                raise e
            print(f'Item with postedOrder "{item["postedOrder"]}" already exists -- not persisting.')
            return {
                'postedOrder': item['postedOrder'],
                'persisted': False
            }


    def put_items(self, items: tp.List[tp.Dict]):
        return list(map(self.put_item, items))


    def put_item(self, item):
        item = json.loads(json.dumps(item), parse_float=Decimal)

        try:
            self.ddb_table.put_item(
                Item=item,
            )
            return {
                'postedOrder': item['postedOrder'],
                'persisted': True
            }
        except ClientError as e:
            logger.error(e)
            return {
                'postedOrder': item['postedOrder'],
                'persisted': False
            }


    def delete_items(self, items: tp.List[tp.Dict]):
        return list(map(self.delete_item, items))
    

    def delete_item(self, item):
        try:
            self.ddb_table.delete_item(
                Key={
                    'postedOrder': item['postedOrder'],
                    'transactionDate': item['transactionDate']
                }
            )
            return {
                'item': item
            }
        except ClientError as e:
            logger.error(e)
            return {
                'item': item,
                'error': True
            }


    def scan_by_transaction_dates(self, from_date, to_date, account_id=None):
        scan_kwargs = {
            'FilterExpression': Attr('transactionDate').between(str(from_date), str(to_date)),
        }
        if account_id:
            scan_kwargs['FilterExpression'] = scan_kwargs['FilterExpression'] & Attr('accountId').eq(account_id)

        done = False
        start_key = None
        transactions = []

        while not done:
            if start_key:
                scan_kwargs['ExclusiveStartKey'] = start_key
            response = self.ddb_table.scan(**scan_kwargs)
            transactions += response.get('Items', [])
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None

        return transactions
