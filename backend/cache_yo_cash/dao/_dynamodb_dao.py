import boto3
import os


class DynamoDBDao():
    def __init__(self, table_name: str) -> None:
        if (os.getenv("CACHE_YO_CASH_LOCAL_TESTING").lower() in ["true", "t", "1"]):
            ddb = boto3.resource('dynamodb', endpoint_url="http://dynamodb-local:8000")
        else:
            ddb = boto3.resource('dynamodb')

        self.ddb_table = ddb.Table(table_name)
