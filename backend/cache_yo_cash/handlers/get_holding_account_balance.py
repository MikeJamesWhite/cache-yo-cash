from datetime import datetime
import logging

from cache_yo_cash.helpers.investec_helper import get_investec_client
from cache_yo_cash.helpers.secrets_helper import get_secret

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event or use default
    logger.info(f"Handling event: {event}")
    if 'accountId' in event:
        account_id = event['accountId']
    else:
        account_id = get_secret("HoldingAccountId")

    # Get investec client
    logger.info("Getting Investec client")
    client = get_investec_client()

    # Get holding account balance and return
    result = client.get_account_balance(account_id)

    return result
