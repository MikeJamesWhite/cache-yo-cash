import dateutil
import logging

from cache_yo_cash.dao.transactions_dao import TransactionsDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    items_list = event['items']

    # Enrich items
    enriched_list = list(map(enrich_item, items_list))

    # Filter invalid items
    valid_items = list(filter(is_item_valid, enriched_list))
    invalid_items = list(filter(lambda id: not is_item_valid(id), enriched_list))

    # Update transactions in DDB
    transactions_dao = TransactionsDao()
    results = transactions_dao.put_items(valid_items)

    return {
        "msg": f"Processed {len(enriched_list)} items.",
        "invalidItems": invalid_items,
        "failedItems": list(filter(lambda res: 'error' in res and res['error'], results)),
        "succeededItems": list(filter(lambda res: 'error' not in res or not res['error'], results))
    }


def enrich_item(item):
    if (type(item['postedOrder']) == str and item['postedOrder'].isnumeric()):
        item['postedOrder'] = int(item['postedOrder'])
    item['transactionDate'] = str(dateutil.parser.parse(item['transactionDate']))
    item['valueDate'] = str(dateutil.parser.parse(item['valueDate']))
    item['actionDate'] = str(dateutil.parser.parse(item['actionDate']))
    item['postingDate'] = str(dateutil.parser.parse(item['postingDate']))
    return item


def is_item_valid(item):
    try:
        assert 'postedOrder' in item and type(item['postedOrder']) == int
        assert 'transactionDate' in item and type(item['transactionDate']) == str
        return True
    except AssertionError as e:
        logger.warn(f"Invalid id: {e}")
        return False
