from datetime import datetime
import logging

from cache_yo_cash.helpers.investec_helper import get_investec_client
from cache_yo_cash.helpers.secrets_helper import get_secret

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event or default
    logger.info(f"Handling event: {event}")
    amount = event['amount']
    if 'mainAccount' in event:
        primary_account = event['mainAccount']
    else:
        primary_account = get_secret("TransactionalAccountId")
    
    if 'holdingAccount' in event:
        holding_account = event['holdingAccount']
    else:
        holding_account = get_secret("HoldingAccountId")

    # Get investec client
    logger.info("Getting Investec client")
    client = get_investec_client()

    # Transfer amount to holding account
    transfer_time = datetime.utcnow()
    result = client.transfer_between_accounts(
        primary_account,
        holding_account,
        amount,
        f'CacheYoCash_TO_HOLDING_{transfer_time}',
        f'CacheYoCash_FROM_PRIMARY_{transfer_time}'
    )

    return result
