import dateutil
import logging
import numbers

from cache_yo_cash.dao.expenses_dao import ExpensesDao
from cache_yo_cash.helpers.uuid_helper import enrich_item_with_uuid

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    expenses_list = event['items']

    # Enrich items
    enriched_list = list(map(enrich_item, expenses_list))

    # Filter invalid items
    valid_items = list(filter(is_item_valid, enriched_list))
    invalid_items = list(filter(lambda item: not is_item_valid(item), enriched_list))

    # Push expenses to DDB
    expenses_dao = ExpensesDao()
    results = expenses_dao.put_items_if_not_exists(valid_items)

    return {
        "msg": f"Processed {len(expenses_list)} items.",
        "invalidItems": invalid_items,
        "failedItems": list(filter(lambda res: 'error' in res and res['error'], results)),
        "succeededItems": list(filter(lambda res: 'error' not in res or not res['error'], results))
    }


def enrich_item(item):
    item['date'] = str(dateutil.parser.parse(item['date']))
    return enrich_item_with_uuid(item)


def is_item_valid(item):
    try:
        assert 'id' in item and type(item['id']) == str
        assert 'date' in item and type(item['date']) == str
        assert 'amount' in item and isinstance(item['amount'], numbers.Number)
        assert (
            'linkedTransactionPostedOrder' in item and type(item['linkedTransactionPostedOrder']) == int) or (
            'linkedTransactionPostedOrder' not in item
        )
        assert 'isDeductible' in item and type(item['isDeductible']) == bool
        assert 'description' in item and type(item['description']) == str
        return True
    except AssertionError as e:
        logger.warn(f"Invalid item: {e}")
        return False
