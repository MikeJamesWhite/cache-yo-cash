import dateutil
import logging

from cache_yo_cash.dao.income_dao import IncomeDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    items_list = event['items']

    # Enrich items
    enriched_list = list(map(enrich_item, items_list))

    # Filter invalid items
    valid_items = list(filter(is_item_valid, enriched_list))
    invalid_items = list(filter(lambda id: not is_item_valid(id), enriched_list))

    # Delete incomes from DDB
    income_dao = IncomeDao()
    results = income_dao.delete_items(valid_items)

    return {
        "msg": f"Processed {len(enriched_list)} items.",
        "invalidItems": invalid_items,
        "failedItems": list(filter(lambda res: 'error' in res and res['error'], results)),
        "succeededItems": list(filter(lambda res: 'error' not in res or not res['error'], results))
    }


def enrich_item(item):
    item['date'] = str(dateutil.parser.parse(item['date']))
    return item


def is_item_valid(item):
    try:
        assert 'id' in item and item['id'] != "" and type(item['id']) == str
        assert 'date' in item and type(item['date']) == str
        return True
    except AssertionError as e:
        logger.warn(f"Invalid id: {e}")
        return False
