import dateutil
import logging

from cache_yo_cash.dao.deductions_dao import DeductionsDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    from_date = dateutil.parser.parse(event['fromDate'])
    to_date = dateutil.parser.parse(event['toDate'])

    # Query DDB for deductions
    deductions_dao = DeductionsDao()
    return {
        'deductions': deductions_dao.scan_by_dates(from_date, to_date)
    }
