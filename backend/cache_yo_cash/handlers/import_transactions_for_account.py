import dateutil
import logging

from cache_yo_cash.helpers.investec_helper import get_investec_client
from cache_yo_cash.helpers.secrets_helper import get_secret
from cache_yo_cash.dao.transactions_dao import TransactionsDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    if 'accountId' in event:
        account_id = event['accountId']
    else:
        account_id = get_secret("TransactionalAccountId")
    from_date = dateutil.parser.parse(event['fromDate'])
    to_date = dateutil.parser.parse(event['toDate'])

    # Get investec client
    logger.info("Getting Investec client")
    client = get_investec_client()

    # Get transactions for period
    logger.info(f"Getting transactions on account '{account_id}' for period from '{from_date}' to '{to_date}'")
    transactions = client.get_account_transactions(
        account_id,
        from_date,
        to_date
    )
    logger.info(f"Got {len(transactions)} transactions")

    # Enrich transactions
    transactions = list(map(enrich_item, transactions))

    # Push transactions to DDB
    transaction_dao = TransactionsDao()
    results = transaction_dao.put_items_if_not_exists(transactions)

    return {
        "msg": f"Processed {len(transactions)} transactions.",
        "results": results
    }


def enrich_item(item):
    item["hasBeenProcessed"] = False
    item['transactionDate'] = str(dateutil.parser.parse(item['transactionDate']))
    item['valueDate'] = str(dateutil.parser.parse(item['valueDate']))
    item['actionDate'] = str(dateutil.parser.parse(item['actionDate']))
    item['postingDate'] = str(dateutil.parser.parse(item['postingDate']))
    return item
