import dateutil
import logging

from cache_yo_cash.dao.transactions_dao import TransactionsDao

# Setup logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger()


def handle(event, context=None):
    # Get arguments from event
    logger.info(f"Handling event: {event}")
    if 'accountId' in event:
        account_id = event['accountId']
    else:
        account_id = None
    from_date = dateutil.parser.parse(event['fromDate'])
    to_date = dateutil.parser.parse(event['toDate'])

    # Query DDB for transactions
    transaction_dao = TransactionsDao()
    return {
        'transactions': transaction_dao.scan_by_transaction_dates(from_date, to_date, account_id)
    }
