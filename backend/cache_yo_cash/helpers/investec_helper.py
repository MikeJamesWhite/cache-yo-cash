from investec.client import InvestecBankingClient
from cache_yo_cash.helpers.secrets_helper import get_secret

def get_investec_client() -> InvestecBankingClient:
    return InvestecBankingClient(
        get_secret("InvestecClientId"),
        get_secret("InvestecSecret")
    )
