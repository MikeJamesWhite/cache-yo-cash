import boto3
import os


def get_secret(secret_id: str) -> str:
    if (os.getenv("CACHE_YO_CASH_LOCAL_TESTING").lower() in ["true", "t", "1"]):
        return os.getenv(f"CACHE_YO_CASH_LOCAL_SECRET_{secret_id}")
    else:
        client = boto3.client('secretsmanager')
        return client.get_secret_value(
            secret_id
        )['SecretString']
