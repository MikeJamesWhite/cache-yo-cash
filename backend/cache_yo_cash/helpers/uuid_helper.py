import uuid

def enrich_item_with_uuid(item, id_attr_name='id'):
    item[id_attr_name] = str(uuid.uuid4())
    return item
