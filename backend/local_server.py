from flask import Flask, request
from flask_cors import CORS

from cache_yo_cash.handlers import (
    get_transactions,
    get_deductions,
    get_expenses,
    get_income,
    add_deductions,
    add_expenses,
    add_income,
    import_transactions_for_account,
    delete_deductions,
    delete_expenses,
    delete_income,
    delete_transactions,
    transfer_to_holding_account,
    get_holding_account_balance,
    update_transactions
)

app = Flask(__name__)
CORS(app)


@app.route("/transactions", methods=['GET', 'POST', 'DELETE', 'PUT'])
def transactions():
    if request.method == 'GET':
        event = {
            'fromDate': request.args['fromDate'],
            'toDate': request.args['toDate'],
        }

        if 'accountId' in request.args:
            event['accountId'] = request.args['accountId']

        return get_transactions.handle(event, {})

    elif request.method == 'POST':
        return import_transactions_for_account.handle(request.get_json(), {})

    elif request.method == 'PUT':
        return update_transactions.handle(request.get_json(), {})

    elif request.method == 'DELETE':
        return delete_transactions.handle(request.get_json(), {})


@app.route("/income", methods=['GET', 'POST', 'DELETE'])
def income():
    if request.method == 'GET':
        event = {
            'fromDate': request.args['fromDate'],
            'toDate': request.args['toDate'],
        }

        return get_income.handle(event, {})

    elif request.method == 'POST':
        return add_income.handle(request.get_json(), {})

    elif request.method == 'DELETE':
        return delete_income.handle(request.get_json(), {})
     

@app.route("/expenses", methods=['GET', 'POST', 'DELETE'])
def expenses():
    if request.method == 'GET':
        event = {
            'fromDate': request.args['fromDate'],
            'toDate': request.args['toDate'],
        }

        return get_expenses.handle(event, {})

    elif request.method == 'POST':
        return add_expenses.handle(request.get_json(), {})

    elif request.method == 'DELETE':
        return delete_expenses.handle(request.get_json(), {})


@app.route("/deductions", methods=['GET', 'POST', 'DELETE'])
def deductions():
    if request.method == 'GET':
        event = {
            'fromDate': request.args['fromDate'],
            'toDate': request.args['toDate'],
        }

        return get_deductions.handle(event, {})

    elif request.method == 'POST':
        return add_deductions.handle(request.get_json(), {})

    elif request.method == 'DELETE':
        return delete_deductions.handle(request.get_json(), {})


@app.route("/holdingAccount", methods=['GET', 'POST'])
def holding_account():
    event = {}
    if 'accountId' in request.args:
        event['accountId'] = request.args['accountId']

    if request.method == 'GET':
        return get_holding_account_balance.handle(event, {})

    elif request.method == 'POST':
        return transfer_to_holding_account.handle(request.get_json(), {})


if __name__ == "__main__":
    app.run(host='0.0.0.0')
