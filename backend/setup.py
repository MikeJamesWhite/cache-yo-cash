import os
import sys

__title__ = "Cache Yo Cash Backend"
__version__ = "1.0.0"
__author__ = "Michael James White"
__authoremail__ = "mike.james.white@icloud.com"
__url__ = "https://gitlab.com/MikeJamesWhite/cache-yo-cash"
__license__ = "MIT License"

if sys.version_info < (3, 8):
    print("%s requires Python 3.8 or later." % __title__)
    sys.exit(1)

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

path = os.path.abspath(os.path.dirname(__file__))
try:
    README = open(os.path.join(path, "README.md")).read()
except IOError:
    README = ""

with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setup(
    name=__title__,
    version=__version__,
    description=f"{__title__} helps businesses leverage programmable banking to keep on top of their tax liabilities",
    long_description=README,
    author=__author__,
    author_email=__authoremail__,
    url=__url__,
    packages=find_packages(exclude=[]),
    include_package_data=True,
    install_requires=requirements,
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    zip_safe=False,
    license=__license__,
    classifiers=[
        "Development Status :: 5 - Stable",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Internet",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ]
)
