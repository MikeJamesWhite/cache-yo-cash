import boto3

# Initialise DDB client
dynamodb_client = boto3.client('dynamodb', endpoint_url="http://dynamodb-local:8000")

def create_table(table_name, hash_key_name, hash_key_type, range_key_name, range_key_type):
    try:
        print(f"Attempting to create table '{table_name}'...")
        dynamodb_client.create_table(
            AttributeDefinitions=[
                {
                    'AttributeName': hash_key_name,
                    'AttributeType': hash_key_type,
                },
                {
                    'AttributeName': range_key_name,
                    'AttributeType': range_key_type,
                },
            ],
            KeySchema=[
                {
                    'AttributeName': hash_key_name,
                    'KeyType': 'HASH',
                },
                {
                    'AttributeName': range_key_name,
                    'KeyType': 'RANGE',
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5,
            },
            TableName=table_name,
        )
        print(f"Successfully created table '{table_name}'")
    except dynamodb_client.exceptions.ResourceInUseException:
        print(f"Table '{table_name}' already exists")


if __name__ == '__main__':
    print("Populating local DDB tables for cache-yo-cash system...")
    create_table("Transactions", "postedOrder", "N", "transactionDate", "S")
    create_table("Income", "id", "S", "date", "S")
    create_table("Expenses", "id", "S", "date", "S")
    create_table("Deductions", "id", "S", "date", "S")
    print("Done populating all tables!")
