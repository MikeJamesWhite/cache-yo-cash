# Cache Yo Cash

Cache Yo Cash uses the power of programmable banking to help businesses keep on top of their tax liabilities. This is my entry for the Investec Q4 2021 Programmable Banking Hackathon.

## Overview
Cache Yo Cash is made up of both backend and frontend components, described below.

### Backend
The Cache Yo Cash backend was developed in Python with the help of [investec-pygrammable-banking](https://gitlab.com/MikeJamesWhite/investec-pygrammable-banking#python-wrapper-for-investec-programmable-banking-open-api).

I was originally planning to deploy everything to AWS Lambda but I realised it made things a lot simpler for local development purposes if I still coded it in the Lambda style (lots of "handlers") but then spun up a lightweight flask server. I also mock DynamoDB using dynamodb-local and Secrets Manager by detecting an override environment variable for local testing (in which case, the secrets are read from the environment rather than calling secrets manager).

Note: In case you need to manually dive into the DDB tables, there is a tool you can run in a terminal and get a DDB admin panel on your machine. Basically just run `npm install -g dynamodb-admin`, then `DYNAMO_ENDPOINT=http://localhost:8000` and finally `dynamodb-admin`.

### Frontend
The Cache Yo Cash frontend was developed in React with MaterialUI as the main components framework. I used Chart.js and react-chartjs-2 for the graphing on the overview page.

Features:
- Overview page
    - View graphs summarising your tax liability situation based on the currently captured income, expense and deduction data.
    - Make transfers from your transactional account to a holding account directly through the UI.
    - Automatically adjust the calculations to your company's financial year by entering the end date of the current financial year.
    - Note, to simplify the task for the hackathon, flat 28% corporate income tax rate is assumed, and assets aren't recorded (though you could probably still factor these in by adding them as a custom expense).
- Transactions page
    - View your transactions in a tabular form and filter by transaction date range.
    - Sync transaction data from your primary business account to the local database by clicking the import button and selecting a date range.
    - Process transactions into incomes or expenses and specify whether they should factor into tax calculations with a simple form. In the case of incomes, you will have the option of automatically transferring all or some of the tax liability associated with that income into a holding account.
- Income page
    - View your income in a tabular form and filter by date range.
    - Manually add income events if there is something that doesn't reflect on the transactions imported from your Investec account.
- Expenses page
    - View your expenses in a tabular form and filter by date range.
    - Manually add expenditure events if there is something that doesn't reflect on the transactions imported from your Investec account.
- Deductions page
    - View your deductions in a tabular form and filter by date range.
    - Manually add deduction events, since these are things that won't appear in your account transaction history.

Note: I don't do much front-end work in my day to day, so the code is probably not pretty! :D

## Getting started

To make it super easy to get started with this project, a couple of Dockerfiles and a docker-compose.yml file are included.

With docker-compose installed, running the examples is as simple as adding credential/configuration values to the environment in the .env file in the root of the project and then running:

`docker-compose build && docker-compose up`

Note: There are some AWS variables in .env, but these are currently only used by dynamodb-local to mock the service, so you don't need to put real credentials here. You will however need to put your Investec credentials and account ids down. I removed the .env file from tracking to avoid anyone mistakenly committing their credentials though :)
